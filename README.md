# ordent_library

## Architecture API

```
.
├── jatis_challenge
│   └── .cert
│   └── api
│   	└── controllers
│   	└── services
│   	└── routes
│   └── cofiguration
│   └── models
│   └── utils
│   	└── docs
│	└── helper
│   	└── swagger
└── .env
└── .env.example
└── .gitignore
└── docker-compose.yml
└── Dockerfile
└── go.mod
└── go.sum
└── main.go
└── README.md
```

Penjelasan :

- .cert = directory untuk menyimpan file key server
- api = directory operational api , dimana ada **controllers**, **routes** dan **services**
  1. Controllers = Directory  untuk implementation services yang tersedia
  2. Routes = Directory untuk routing load proses di controllers
  3. services = Directory yag berisi function untuk di jalankan di controllers
- configuration =directory load connection database dari config env
- models = directory yang berisi struct 
- utils = directoy untuk menyimpan thirparty keperluan proses API diantaranya **docs** , **helper**, **swagger**
  1. Docs = directory tempat menyimpan documentation api di swagger
  2. helper = directory untuk menyimpan function tambahan  atau thirparty penunjang api
  3. swagger = directory untuk load template swwagger documentation API

# Config Database (Env)

1. buat file.env dengan format contoh di file .env.example
2. Pastikan database telah di buat terlebih dahulu , misalnya **jatis_challenge**.

# Installation (LOCAL)

## Menggunakan AIR (Direkomendasikan)

1. Jika sudah terinstal, lanjut poin(2). Instal cosmtrek/air(direkomendasikan) dengan mengikuti panduan pada https://github.com/cosmtrek/air#installation
2. Pastikan value **APP_MODE** pada file _.env_ adalah LOCAL.
3. Ketik `air` pada terminal/cmd untuk menjalankan Api.
4. API akan berjalan pada port 8080 secara default.
5. Sehingga API akan dapat diakses pada http://localhost:8080

## Menggunakan GO RUN

1. Pastikan value **APP_MODE** pada file _.env_ adalah DEBUG.
2. Ketik `go run main.go` pada terminal/cmd untuk menjalankan API.
3. API akan berjalan pada port 8080 secara default.
4. Sehingga API akan dapat diakses pada http://localhost:8080

## Menggunakan Docker Compose

1. Pastikan value **APP_MODE** pada file _.env_ adalah DEBUG.
2. Build API dengan mengetik `docker compose up -d` pada terminal/cmd.
3. Tadaaa, API berjalan pada port 8080 dan dapat diakses pada http://localhost:8080.

# Menjalankan API (STAGING)

## Menggunakan Docker

1. Pastikan value **APP_MODE** pada file _.env_ adalah STAGING.
2. Hapus Container lalu Image API yang terinstall sebelumnya pada docker melalui portainer pada http://xxx.xx.co.id:9000/.
3. Build API menjadi docker file dengan mengetik `sudo docker image build -t ordent_library.` pada terminal/cmd.
4. Setelah proses build berhasil, waktunya untuk menjalankan image dari API menjadi container dengan mengetik `sudo docker container run --name ordent_library-p 7000:8080 -d ordent-staging` pada terminal/cmd.
5. Maka otomatis API akan berjalan pada port 8080 dan dapat diakses pada https://xxx.xx.co.id:7000.

# Menjalankan API (PRODUCTION)

## Menggunakan Docker Compose

1. Pastikan value **APP_MODE** pada file _.env_ adalah PRODUCTION.

2. Build API dengan mengetik `docker compose up -d` pada terminal/cmd.

3. API berjalan pada port 8080 dan dapat diakses pada https://api.xxx.co.id:8080.

   

# Configuration Auth X-api-key untuk akses Documentation Api di Swagger

1. Jalankan program.

2. Buka Database.

3. Insert manual data di table security , field security isi dengan **x-api-key** dan field **values** di isi dengan passwordnya.

   https://i.imgur.com/mnQQ9my.png



# Configuration Untuk Mengakses Documentation Api di Swagger menu Peminjaman dan pengembalian buku

1. Jalankan program.

2. masuk ke swagger http://localhost:8080/swagger/index.html "jika local" dan masuk dengan auth **x-api-key** yang telah di setting sebelumya.

3. buat akun di menu login config buat akun login ke api dengan login basic auth.

   https://i.imgur.com/mnQQ9my.png
