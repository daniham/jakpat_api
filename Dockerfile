FROM golang:alpine

RUN apk update && apk add --no-cache git

RUN mkdir -p /app
WORKDIR /app
COPY go.mod /app
COPY go.sum /app
RUN go mod download

COPY . /app
RUN go mod tidy

ENV TZ="Asia/Jakarta"
RUN go build -o ./app/ordent_library

ENTRYPOINT ["./app/ordent_library"]